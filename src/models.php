<?php
/**
 * Created by PhpStorm.
 * User: Maxime SAUTAREL
 * Date: 04/04/2019
 * Time: 16:00
 */

class Voiture extends Model{
    public static $_table = 'Voiture';
    public static $_id_column = 'ID';
}

class Contact extends Model{
    public static $_table = 'Contact';
}