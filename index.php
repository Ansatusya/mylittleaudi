<?php
/**
 * Created by PhpStorm.
 * User: Maxime SAUTAREL
 * Date: 04/04/2019
 * Time: 14:24
 */

require_once "vendor/autoload.php";

session_start();

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    });

Flight::before('start', function($params, $output){
    ORM::configure('sqlite:audi.db');
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function () {
    $data = [
        'voitures' => Voiture::find_many(),
    ];
    Flight::render('voiture.twig', $data);
});

Flight::route('/voiture/@id', function ($id) {
    $data = [
        'voiture' => Voiture::find_one($id),
    ];
    Flight::render('details.twig', $data);
});

Flight::route('/contact', function(){
    $contact = Model::factory('Contact')->create();
    $contact->NOM = null;
    $contact->MESSAGE = null;

    if(Flight::request()->method == 'POST')
    {

        $contact->NOM = Flight::request()->data->name;
        $contact->MESSAGE = Flight::request()->data->message;
        $contact->save();
        Flight::redirect('/contact');

    }
else
{
    Flight::render('contact.twig');
}

});

Flight::route('/admin', function() {
        $data = [
            'voitures' => Voiture::find_many()
        ];
        Flight::render('admin/home.twig', $data);
});

Flight::route('/voitures/form(/@id)', function ($id) {
    if ($id) {
        $voiture = Model::factory('Voiture')->find_one($id);
    } else {
        $voiture = Model::factory('Voiture')->create();
        $voiture->MODELE = null;
        $voiture->PRIX = null;
        $voiture->PUISSANCE = null;
        $voiture->COUPLE = null;
        $voiture->VITESSE_MAX = null;
        $voiture->ACCELERATION = null;
        $voiture->ENERGIE = null;
        $voiture->MOTEUR = null;
    }


    //2. if method is POST
    if (Flight::request()->method == 'POST') {
        $voiture->MODELE = Flight::request()->data->model;
        $voiture->PRIX = Flight::request()->data->price;
        $voiture->PUISSANCE = Flight::request()->data->power;
        $voiture->COUPLE = Flight::request()->data->couple;
        $voiture->VITESSE_MAX = Flight::request()->data->speed;
        $voiture->ACCELERATION = Flight::request()->data->acceleration;
        $voiture->ENERGIE = Flight::request()->data->energy;
        $voiture->MOTEUR = Flight::request()->data->engine;
        $voiture->save();
        Flight::redirect('/admin');
    } else {
        $data = [
            'voiture' => $voiture,
        ];
        Flight::render('admin/form.twig', $data);
    }
});

Flight::route('/voitures/delete/@id', function($id){
    $voiture = Model::factory('Voiture')->find_one($id);
    $voiture->delete();
    Flight::redirect('/admin');
});

Flight::route('/admin/contact', function(){
     $data = [
         'contacts' => Contact::find_many(),
     ];
    Flight::render('/admin/contacts.twig', $data);
});


Flight::start();