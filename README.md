# ![](assets/img/final transparent.png)



## How to ?

1. `composer install`
2. `vendor/bin/phpunit tests/`
3. php -S localhost:8000
4. Et go sur [localhost:8000](http://localhost:8000)
5. Pour l'interface d'administration rendez-vous sur [localhost:8000/admin](http://localhost:8000/admin)


## Requirements

```
mikecao/flight,
symfony/process,
guzzlehttp/guzzle,
phpunit/phpunit,
twig/twig,
rmccue/requests,
michelf/php-markdown,
j4mie/paris
tamtamchik/simple-flash
```

## Authors

> Maxime SAUTAREL

> Loann MOULIN